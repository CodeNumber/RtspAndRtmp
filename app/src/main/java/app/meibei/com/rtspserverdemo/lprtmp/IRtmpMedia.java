package app.meibei.com.rtspserverdemo.lprtmp;

import android.media.MediaCodec;

public interface IRtmpMedia {
    void startRtmp(String url, int width, int height);
    void stopRtmp();
    void inputBuffer(byte[] data);

}
