package app.meibei.com.rtspserverdemo.lprtmp;

import android.hardware.Camera;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 接收处理摄像头返回的NV21 byte数组
 * */
public class FaceRtmpPrviewCallback implements CameraUtil.PreviewCallback {
    ExecutorService executor = Executors.newSingleThreadExecutor();
    private RtmpMediaCodec rtmpMediaCodec;


    public FaceRtmpPrviewCallback(RtmpMediaCodec rtmpMediaCodec) {
        this.rtmpMediaCodec=rtmpMediaCodec;
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        //将nv21数据放入MeidaCodec编码器进行输入编码
        long endTime = System.currentTimeMillis();
        if(rtmpMediaCodec!=null){
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    rtmpMediaCodec.inputBuffer(data);
                    Log.e("编码耗时",  String.valueOf(System.currentTimeMillis()-endTime));

                }
            });

        }

    }
}
