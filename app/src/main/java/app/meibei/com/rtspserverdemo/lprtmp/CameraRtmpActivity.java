package app.meibei.com.rtspserverdemo.lprtmp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import app.meibei.com.rtspserverdemo.R;

/**
 * CameraRtmpActivity
 * 用于启动和预览相机，并通过人脸识别系统，标记人脸框信息
 * 再由Mediacodec硬编码为H.264发送给再用librtmp推流到ngix服务器
 */
public class CameraRtmpActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";
    private SurfaceView cameraSurface;
    private CameraUtil mCameraUtil;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private Camera mCamera;
    private int mCameraId = 0;
    private int mWidth = 640;
    private int mHeight = 480;
    private SurfaceHolder mHolder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_rtmp);
        initViews();
        initPermision();
    }


    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }
    private void initPermision() {
        //检查权限和硬件
        if (ContextCompat.checkSelfPermission(
                CameraRtmpActivity.this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(CameraRtmpActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(CameraRtmpActivity.this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CAMERA_PERMISSION);
        } else {
            if (!checkCameraHardware(this)) {
                Log.i(TAG, "没有检测到相机硬件");
            } else {
                initCamera();
            }

        }
    }

    private void initViews() {
        cameraSurface =findViewById(R.id.camera_surface);

    }
    private void initCamera() {
        mHolder = cameraSurface.getHolder();
        mHolder.addCallback(new FaceRtmpHolderCallback(this,mHolder));

    }

}
