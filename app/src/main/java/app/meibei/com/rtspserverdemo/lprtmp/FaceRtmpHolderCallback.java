package app.meibei.com.rtspserverdemo.lprtmp;


import android.app.Activity;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.IOException;

public class FaceRtmpHolderCallback implements SurfaceHolder.Callback {
    private static final String TAG="FaceRtmpHolderCallback";
    private CameraUtil mCameraUtil;
    private Camera mCamera;
    private int mCameraId = 0;
    private int mWidth = Constans.RTMP_WIDTH;
    private int mHeight = Constans.RTMP_HEIGHT;
    private Activity mActivity;
    private SurfaceHolder mHolder;
    private RtmpMediaCodec rtmpMediaCodec=new RtmpMediaCodec();

    public FaceRtmpHolderCallback(Activity mActivity, SurfaceHolder mHolder) {
        this.mActivity = mActivity;
        this.mHolder = mHolder;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.e(TAG, "surfaceCreated: ");
        mCamera = Camera.open(mCameraId);
        mCameraUtil = new CameraUtil(mCamera, mCameraId);
        rtmpMediaCodec.startRtmp(Constans.RTMP_URL,Constans.RTMP_WIDTH,Constans.RTMP_HEIGHT);

    }
    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        Log.e(TAG, "surfaceChanged: ");
        mCameraUtil.initCamera(mWidth, mHeight, mActivity);
        mCameraUtil.setPreviewCallback(new FaceRtmpPrviewCallback(rtmpMediaCodec));
        try {
            mCamera.setPreviewDisplay(mHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.e(TAG, "surfaceDestroyed: ");
        mCameraUtil.stopPreview();
        rtmpMediaCodec.stopRtmp();
    }
}
